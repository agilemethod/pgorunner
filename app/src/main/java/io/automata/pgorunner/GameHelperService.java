package io.automata.pgorunner;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.WindowManager;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import net.grandcentrix.tray.AppPreferences;
import net.grandcentrix.tray.core.OnTrayPreferenceChangeListener;
import net.grandcentrix.tray.core.TrayItem;

import java.util.Collection;

import io.automata.pgorunner.model.GpsModel;
import io.automata.pgorunner.model.GpsModelClient;
import io.automata.pgorunner.model.GpsPlayer;
import io.automata.pgorunner.model.GpsPlayerImpl;
import io.automata.pgorunner.model.PokemonModel;
import io.automata.pgorunner.model.SettingModel;
import io.automata.pgorunner.model.mock.PokemonModelMock;
import io.automata.pgorunner.ui.JoystickView;
import io.automata.pgorunner.ui.MainMenuView;
import io.automata.pgorunner.ui.WorkView;

public class GameHelperService extends Service {

    private WindowManager mWindowManager;
    private LayoutInflater mLayoutInflater;
    private MainMenuView mMainMenuView;
    private WorkView mWorkView;
    private JoystickView mJoystickView;
    private GpsModel mGpsModel;
    private GpsPlayer mGpsPlayer;
    private PokemonModel mPokemonModel;
    private AppPreferences mAppPreferences;
    private OnTrayPreferenceChangeListener mOnTrayPreferenceChangeListener;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mLayoutInflater = LayoutInflater.from(getApplicationContext());

        initModel();
        initViewer();
        initPresenter();
    }

    private void initModel() {
        mGpsModel = new GpsModelClient(getBaseContext());
        mGpsModel.setFakeLocation(mGpsModel.getHomeLocation().getLatitude(), mGpsModel.getHomeLocation().getLongitude());
        mGpsPlayer = new GpsPlayerImpl(mGpsModel);
        mPokemonModel = new PokemonModelMock();
        mAppPreferences = new AppPreferences(getBaseContext());
        mAppPreferences.put(SettingModel.SERVICE_ENABLE, true);
    }

    private void initViewer() {
        mMainMenuView = (MainMenuView) mLayoutInflater.inflate(R.layout.main_menu_view, null, false);
        mMainMenuView.init(mWindowManager, mLayoutInflater);
        mMainMenuView.show();

        mWorkView = (WorkView) mLayoutInflater.inflate(R.layout.work_view, null, false);
        mWorkView.init(mWindowManager, mLayoutInflater);

        // FIXME: setHomeLocation for test
        mWorkView.setFakeLocation(mGpsModel.getHomeLocation());
        mWorkView.setTargetLocation(mGpsModel.getHomeLocation());

        mJoystickView = (JoystickView) mLayoutInflater.inflate(R.layout.joystick_view, null, false);
        mJoystickView.init(mWindowManager, mLayoutInflater);
        mJoystickView.show();
        mJoystickView.setDraggable(false);
    }

    private void initPresenter() {
        mMainMenuView.setEventListener(new MainMenuView.EventListener() {
            @Override
            public void OnVisibleChanged() {
                mJoystickView.setDraggable(!mMainMenuView.getVisible());
                mWorkView.hide();
            }

            @Override
            public void OnMapIconClicked() {
                if (mWorkView.getVisible() && mWorkView.getMode() == WorkView.ModeType.MAP) {
                    mWorkView.hide();
                } else {
                    mWorkView.setMode(WorkView.ModeType.MAP);
                    mWorkView.show();
                }
            }

            @Override
            public void OnIvIconClicked() {
                if (mWorkView.getVisible() && mWorkView.getMode() == WorkView.ModeType.IV) {
                    mWorkView.hide();
                } else {
                    mWorkView.setMode(WorkView.ModeType.IV);
                    mWorkView.show();
                    mWorkView.setPokemonText(mPokemonModel.getPokemonText());
                }
            }

            @Override
            public void OnSpeedIconClicked() {
                mGpsPlayer.setSpeed(mMainMenuView.getMovingSpeed());
            }

            @Override
            public void OnNavigateIconClicked() {
                if (mGpsPlayer.getWalking() == false) {
                    mGpsPlayer.walkTo(mWorkView.getTargetLocation());
                } else {
                    mGpsPlayer.stopWalking();
                }
            }

            @Override
            public void OnCloseIconClicked() {
                mAppPreferences.put(SettingModel.SERVICE_ENABLE, false);
                stopSelf();
            }
        });

        mWorkView.setEventListener(new WorkView.EventListener() {
            @Override
            public void OnMapWalkButtonClicked() {
                mGpsPlayer.walkTo(mWorkView.getTargetLocation());
            }

            @Override
            public void OnTargetLocationChanged() {
                if (mGpsPlayer.getWalking()) {
                    mGpsPlayer.walkTo(mWorkView.getTargetLocation());
                }
            }

            @Override
            public void OnFakeLocationChanged() {
                // FIXME
            }

            @Override
            public void OnMapCameraIdle(LatLngBounds bounds) {
                // FIXME
                /*
                PokemonMapModel.query(bounds, callback);
                 */
            }
        });

        mJoystickView.setEventListener(new JoystickView.EventListener() {
            @Override
            public void OnJoystickUpdated() {
            if (mGpsPlayer.getWalking()) {
                mGpsPlayer.stopWalking();
            }

            LatLng latLng = mGpsPlayer.calcLatLngByDegree(mGpsModel.getFakeLocation(), mJoystickView.getDegree());
            mGpsModel.setFakeLocation(latLng.latitude, latLng.longitude);
            }
        });

        mGpsPlayer.setEventListener(new GpsPlayer.EventListener() {
            @Override
            public void OnWalkStarted() {
                mMainMenuView.setState(MainMenuView.IconType.NAVIGATE, 1);
            }

            @Override
            public void OnWalkStopped() {
                mMainMenuView.setState(MainMenuView.IconType.NAVIGATE, 0);
            }
        });

        mGpsModel.setEventListener(new GpsModel.EventListener() {
            @Override
            public void OnFakeLocationChanged() {
                mWorkView.setFakeLocation(mGpsModel.getFakeLocation());
            }
        });
    }

    @Override public void onDestroy() {
        if (mGpsModel instanceof  GpsModelClient) {
            ((GpsModelClient) mGpsModel).destroy();
        }

        mMainMenuView.destroy();
        mWorkView.destroy();
        mJoystickView.destroy();

        super.onDestroy();
    }

    private OnTrayPreferenceChangeListener handleOnTrayPreferenceChange() {
        if (mOnTrayPreferenceChangeListener == null) {
            mOnTrayPreferenceChangeListener = new OnTrayPreferenceChangeListener() {
                @Override
                public void onTrayPreferenceChanged(Collection<TrayItem> items) {

                }
            };
        }
        return mOnTrayPreferenceChangeListener;
    }

    // =======================================================
    // mocks
}