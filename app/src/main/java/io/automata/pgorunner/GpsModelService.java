package io.automata.pgorunner;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;

import net.grandcentrix.tray.AppPreferences;
import net.grandcentrix.tray.core.OnTrayPreferenceChangeListener;
import net.grandcentrix.tray.core.TrayItem;

import java.util.Collection;

import io.automata.pgorunner.model.GpsModel;
import io.automata.pgorunner.utility.Tool;

public class GpsModelService extends Service {

    private AppPreferences mAppPreferences;
    private OnTrayPreferenceChangeListener mPreferenceChangedHandler;
    private Location mHomeLocation = new Location("FIXME");

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mAppPreferences = new AppPreferences(getBaseContext());
        mAppPreferences.registerOnTrayPreferenceChangeListener(handlePreferenceChanged());
    }

    @Override public void onDestroy() {
        mAppPreferences.unregisterOnTrayPreferenceChangeListener(handlePreferenceChanged());
        super.onDestroy();
    }

    public Location getHomeLocation() {
        return mHomeLocation;
    }

    private OnTrayPreferenceChangeListener handlePreferenceChanged() {
        if (mPreferenceChangedHandler == null) {
            mPreferenceChangedHandler = new OnTrayPreferenceChangeListener() {
                @Override
                public void onTrayPreferenceChanged(Collection<TrayItem> items) {
                    for (TrayItem item : items) {
                        switch (item.key()) {
                            case GpsModel.CMD_SET_FAKE_LOC:
                                handleSetFakeLocation(item.value());
                                break;
                        }
                    }
                }
            };
        }
        return mPreferenceChangedHandler;
    }

    private void handleSetFakeLocation(String value) {
        try {
            LatLng latLng = Tool.latLngFromText(value);
            mAppPreferences.put(GpsModel.FAKE_LAT, (float) latLng.latitude);
            mAppPreferences.put(GpsModel.FAKE_LNG, (float) latLng.longitude);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }
}
