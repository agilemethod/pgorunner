package io.automata.pgorunner;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import net.grandcentrix.tray.AppPreferences;
import net.grandcentrix.tray.core.OnTrayPreferenceChangeListener;
import net.grandcentrix.tray.core.TrayItem;

import java.util.Collection;

import io.automata.pgorunner.model.GpsModel;
import io.automata.pgorunner.model.GpsModelClient;
import io.automata.pgorunner.model.SettingModel;
import io.automata.pgorunner.utility.Tool;

public class PGoRunnerActivity extends PreferenceActivity {

    private boolean mIsAllowFloatingWindow = false;
    private final static int OVERLAY_PERMISSION_REQ_CODE = 2428;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new MainPreferenceFragment(this))
                .commit();
    }

    public static class MainPreferenceFragment extends PreferenceFragment implements OnTrayPreferenceChangeListener {
        private PGoRunnerActivity PGoRunnerActivity;
        private GpsModel mGpsModel;
        private AppPreferences mAppPreferences;
        private SwitchPreference mServiceEnable;
        private SwitchPreference mHomeLocDraggable;
        private EditTextPreference mFakeLocChange;
        private EditTextPreference mHomeLocReset;
        private SwitchPreference mPokemonMapEnable;
        private SwitchPreference mFloatingMapEnable;

        public MainPreferenceFragment(PGoRunnerActivity pGoRunnerActivity) {
            PGoRunnerActivity = pGoRunnerActivity;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mAppPreferences = new AppPreferences(getActivity());

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.pgorunner_preference);

            mServiceEnable = (SwitchPreference) findPreference(SettingModel.SERVICE_ENABLE);
            mServiceEnable.setOnPreferenceChangeListener(handleServiceEnableChanged());

            mFakeLocChange = (EditTextPreference) findPreference(SettingModel.FAKE_LOC_CHANGE);
            mFakeLocChange.setOnPreferenceChangeListener(handleFakeLocChangeChanged());

            mHomeLocReset = (EditTextPreference) findPreference(SettingModel.FAKE_LOC_RESET);
            mHomeLocReset.setOnPreferenceChangeListener(handleFakeLocResetChanged());

            mHomeLocDraggable = (SwitchPreference) findPreference(SettingModel.FAKE_LOC_DRAGGABLE);
            mHomeLocDraggable.setOnPreferenceChangeListener(handleFakeLocDraggableChanged());

            mPokemonMapEnable = (SwitchPreference) findPreference(SettingModel.POKEMON_MAP_ENABLE);
            mPokemonMapEnable.setOnPreferenceChangeListener(handlePokemonMapEnableChanged());

            mFloatingMapEnable = (SwitchPreference) findPreference(SettingModel.FLOATING_MAP_ENABLE);
            mFloatingMapEnable.setOnPreferenceChangeListener(handleFloatingMapEnableChanged());

            mGpsModel = new GpsModelClient(PGoRunnerActivity.getBaseContext());
            mGpsModel.setEventListener(new GpsModel.EventListener() {
                @Override
                public void OnFakeLocationChanged() {
                    updateUI();
                }
            });

            initUI();
        }

        private Preference.OnPreferenceChangeListener handleServiceEnableChanged() {
            return new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    if ((Boolean) newValue == true) {
                        if (PGoRunnerActivity.floatingWindowIsAllowed()) {
                            PGoRunnerActivity.startService();
                        } else {
                            PGoRunnerActivity.requestPermissionForFloatingWindow();
                        }
                        return false;
                    } else {
                        PGoRunnerActivity.stopService();
                        mAppPreferences.put(SettingModel.SERVICE_ENABLE, false);
                        return true;
                    }

                }
            };
        }

        private Preference.OnPreferenceChangeListener handleFakeLocChangeChanged() {
            return new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    try {
                        final LatLng latLng = Tool.latLngFromText((String) newValue);
                        mGpsModel.setFakeLocation(latLng.latitude, latLng.longitude);
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    }
                    return false;
                }
            };
        }

        private void updateFakeLocChangeUI(LatLng latLng) {
            final String newFakeLoc = Tool.textFromLatLng(latLng);
            mFakeLocChange.setText(newFakeLoc);
            mFakeLocChange.setSummary(newFakeLoc);
        }

        private Preference.OnPreferenceChangeListener handleFakeLocResetChanged() {
            return new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String value = (String) newValue;
                    if (value.toLowerCase().equals("yes")) {
                        final double lat = mGpsModel.getHomeLocation().getLatitude();
                        final double lng = mGpsModel.getHomeLocation().getLongitude();
                        mGpsModel.setFakeLocation(lat, lng);
                    }
                    return false;
                }
            };
        }

        private Preference.OnPreferenceChangeListener handleFakeLocDraggableChanged() {
            return new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    mAppPreferences.put(SettingModel.FAKE_LOC_DRAGGABLE, (Boolean) newValue);
                    return true;
                }
            };
        }

        private Preference.OnPreferenceChangeListener handlePokemonMapEnableChanged() {
            return new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    mAppPreferences.put(SettingModel.POKEMON_MAP_ENABLE, (Boolean) newValue);
                    return true;
                }
            };
        }

        private Preference.OnPreferenceChangeListener handleFloatingMapEnableChanged() {
            return new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    mAppPreferences.put(SettingModel.FLOATING_MAP_ENABLE, (Boolean) newValue);
                    return true;
                }
            };
        }

        private void initUI() {
            mServiceEnable.setChecked(mAppPreferences.getBoolean(SettingModel.SERVICE_ENABLE, false));
            final LatLng latLng = new LatLng(mGpsModel.getFakeLocation().getLatitude(), mGpsModel.getFakeLocation().getLongitude());
            updateFakeLocChangeUI(latLng);
            mHomeLocDraggable.setChecked(mAppPreferences.getBoolean(SettingModel.FAKE_LOC_DRAGGABLE, false));
            mPokemonMapEnable.setChecked(mAppPreferences.getBoolean(SettingModel.POKEMON_MAP_ENABLE, false));
            mFloatingMapEnable.setChecked(mAppPreferences.getBoolean(SettingModel.FLOATING_MAP_ENABLE, false));
        }

        @Override
        public void onResume() {
            super.onResume();
            if (mAppPreferences != null) {
                mAppPreferences.registerOnTrayPreferenceChangeListener(this);
            }
            updateUI();
        }

        private void updateUI() {
            final LatLng latLng = new LatLng(mGpsModel.getFakeLocation().getLatitude(), mGpsModel.getFakeLocation().getLongitude());
            updateFakeLocChangeUI(latLng);
        }

        @Override
        public void onPause() {
            if (mAppPreferences != null) {
                mAppPreferences.registerOnTrayPreferenceChangeListener(this);
            }
            super.onPause();
        }

        @Override
        public void onDestroy() {
            if (mAppPreferences != null) {
                mAppPreferences.registerOnTrayPreferenceChangeListener(this);
            }

            if (mGpsModel instanceof GpsModelClient) {
                ((GpsModelClient) mGpsModel).destroy();
            }

            super.onDestroy();
        }

        @Override
        public void onTrayPreferenceChanged(Collection<TrayItem> items) {
            for (TrayItem item : items) {
                switch (item.key()) {
                    case SettingModel.SERVICE_ENABLE:
                        boolean value = item.value().equals("true");
                        if (mServiceEnable.isChecked() != value) {
                            mServiceEnable.setChecked(value);
                        }
                        break;
                }
            }
        }
    }

    private void startService() {
        startService(new Intent(getBaseContext(), GameHelperService.class));
    }

    private void stopService() {
        stopService(new Intent(getBaseContext(), GameHelperService.class));
    }

    private boolean floatingWindowIsAllowed() {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(this)) {
                mIsAllowFloatingWindow = false;
            } else {
                mIsAllowFloatingWindow = true;
            }
        } else {
            mIsAllowFloatingWindow = true;
        }

        return mIsAllowFloatingWindow;
    }

    private void requestPermissionForFloatingWindow() {
        final int resourceID = getResources().getIdentifier("hint_overlay", "string", getPackageName());
        Toast.makeText(this, getString(resourceID), Toast.LENGTH_LONG).show();

        final Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                Uri.parse("package:" + getPackageName()));

        startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
    }
}
