package io.automata.pgorunner.model;

import android.location.Location;

public interface GpsModel {

    Location getHomeLocation();

    Location getFakeLocation();

    void setEventListener(EventListener eventListener);

    void setFakeLocation(double latitude, double longitude);

    interface EventListener {
        void OnFakeLocationChanged();
    }

    String CMD_SET_FAKE_LOC = "CMD_SET_FAKE_LOC";
    String HOME_LAT = "GM_HOME_LAT";
    String HOME_LNG = "GM_HOME_LNG";
    String FAKE_LAT = "GM_FAKE_LAT";
    String FAKE_LNG = "GM_FAKE_LNG";
 }
