package io.automata.pgorunner.model;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;

import net.grandcentrix.tray.AppPreferences;
import net.grandcentrix.tray.core.OnTrayPreferenceChangeListener;
import net.grandcentrix.tray.core.TrayItem;

import java.util.Collection;

import io.automata.pgorunner.GpsModelService;

public class GpsModelClient implements GpsModel {

    private final AppPreferences mAppPreferences;
    private final Location mFakeLocation;
    private final Location mHomeLocation;
    private OnTrayPreferenceChangeListener mPreferenceChangedHandler;
    private EventListener mEventListener;
    private boolean mFakeLatChanged = false;
    private boolean mFakeLngChanged = false;

    public GpsModelClient(Context context) {
        context.startService(new Intent(context, GpsModelService.class));

        mAppPreferences = new AppPreferences(context);
        mAppPreferences.registerOnTrayPreferenceChangeListener(handlePreferenceChanged());

        mFakeLocation = new Location("gps");
        mFakeLocation.setLongitude(mAppPreferences.getFloat(GpsModel.FAKE_LAT, 0));
        mFakeLocation.setLatitude(mAppPreferences.getFloat(GpsModel.FAKE_LNG, 0));
        mFakeLocation.setAccuracy(Criteria.ACCURACY_COARSE);

        mHomeLocation = new Location("gps");
        mHomeLocation.setTime(System.currentTimeMillis());
        mHomeLocation.setLatitude(25.047565);
        mHomeLocation.setLongitude(121.516996);
        mHomeLocation.setAccuracy(Criteria.ACCURACY_COARSE);
    }

    public void destroy() {
        mAppPreferences.unregisterOnTrayPreferenceChangeListener(handlePreferenceChanged());
    }

    private OnTrayPreferenceChangeListener handlePreferenceChanged() {
        if (mPreferenceChangedHandler == null) {
            mPreferenceChangedHandler = new OnTrayPreferenceChangeListener() {
                @Override
                public void onTrayPreferenceChanged(Collection<TrayItem> items) {
                    for (TrayItem item : items) {
                        switch (item.key()) {
                            case GpsModel.FAKE_LAT:
                                mFakeLocation.setLatitude(Float.parseFloat(item.value()));
                                mFakeLatChanged = true;
                                tryToEmitFakeLocationChanged();
                                break;
                            case GpsModel.FAKE_LNG:
                                mFakeLocation.setLongitude(Float.parseFloat(item.value()));
                                mFakeLngChanged = true;
                                tryToEmitFakeLocationChanged();
                                break;
                        }
                    }
                }
            };
        }
        return mPreferenceChangedHandler;
    }

    private void tryToEmitFakeLocationChanged() {
        if (mEventListener != null) {
            if (mFakeLatChanged && mFakeLngChanged) {
                mEventListener.OnFakeLocationChanged();
                mFakeLatChanged = mFakeLngChanged = false;
            }
        }
    }

    @Override
    public Location getHomeLocation() {
        return mHomeLocation;
    }

    @Override
    public Location getFakeLocation() {
        return mFakeLocation;
    }

    @Override
    public void setEventListener(EventListener eventListener) {
        mEventListener = eventListener;
    }

    @Override
    public void setFakeLocation(double latitude, double longitude) {
        mAppPreferences.put(GpsModel.CMD_SET_FAKE_LOC, (float) latitude + ", " + (float) longitude);
    }
}
