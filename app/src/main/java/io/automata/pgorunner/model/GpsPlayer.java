package io.automata.pgorunner.model;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

public interface GpsPlayer {

    void setSpeed(int movingSpeed);

    boolean getWalking();

    void walkTo(Location targetLocation);

    void stopWalking();

    void setEventListener(EventListener eventListener);

    LatLng calcLatLngByDegree(Location location, double degree);

    interface EventListener {
        void OnWalkStarted();

        void OnWalkStopped();
    }
}
