package io.automata.pgorunner.model;

import android.location.Location;
import android.os.Handler;

import com.google.android.gms.maps.model.LatLng;

public class GpsPlayerImpl implements GpsPlayer {

    private final GpsModel mGpsModel;
    private boolean mWalking = false;
    private EventListener mEventListener;
    private Location mTargetLocation;
    public float mBasicSpeed = 2.04f; // unit: m/s
    public int mSpeedRatio = 1;
    private Handler mHandler;
    private Runnable mWalkByStepTask;

    public GpsPlayerImpl(GpsModel gpsModel) {
        mHandler = new Handler();
        mGpsModel = gpsModel;
    }

    @Override
    public void setSpeed(int movingSpeed) {
        mSpeedRatio = movingSpeed;
    }

    @Override
    public boolean getWalking() {
        return mWalking;
    }

    @Override
    public void walkTo(Location targetLocation) {
        if (mWalkByStepTask == null) {
            mWalkByStepTask = new Runnable() {
                @Override
                public void run() {
                    walkByStep();
                }
            };
        }

        mTargetLocation = targetLocation;

        mHandler.removeCallbacks(mWalkByStepTask);
        mHandler.postDelayed(mWalkByStepTask, 100);

        mWalking = true;
        if (mEventListener != null) {
            mEventListener.OnWalkStarted();
        }
    }

    private void walkByStep() {
        Location fakeLocation = mGpsModel.getFakeLocation();
        float dis = fakeLocation.distanceTo(mTargetLocation);
        if (dis > 10) {
            float needTime = dis / (mBasicSpeed * mSpeedRatio);
            double newLat = fakeLocation.getLatitude()
                    + (mTargetLocation.getLatitude() - fakeLocation.getLatitude()) / needTime;
            double newLng = fakeLocation.getLongitude()
                    + (mTargetLocation.getLongitude() - fakeLocation.getLongitude()) / needTime;
            mGpsModel.setFakeLocation(newLat, newLng);

            mHandler.postDelayed(mWalkByStepTask, 1000);
        } else {
            mGpsModel.setFakeLocation(fakeLocation.getLatitude(), fakeLocation.getLongitude());
            stopWalking();
        }
    }

    @Override
    public void stopWalking() {
        mHandler.removeCallbacks(mWalkByStepTask);
        mWalking = false;

        if (mEventListener != null) {
            mEventListener.OnWalkStopped();
        }
    }

    @Override
    public void setEventListener(EventListener eventListener) {
        mEventListener = eventListener;
    }

    @Override
    public LatLng calcLatLngByDegree(Location location, double degree) {
        final float distanceInMeters = mBasicSpeed * mSpeedRatio;
        return destVincenty(location.getLatitude(), location.getLongitude(), degree, distanceInMeters);
    }

    private static LatLng destVincenty(final double latitude, final double longitude, final double angle, final double distanceInMeters) {
        // WGS-84 ellipsiod
        final double semiMajorAxis = 6378137;
        final double b = 6356752.3142;
        final double inverseFlattening = 1 / 298.257223563;
        final double alpha1 = Math.toRadians(angle);
        final double sinAlpha1 = Math.sin(alpha1);
        final double cosAlpha1 = Math.cos(alpha1);

        final double tanU1 = (1 - inverseFlattening) * Math.tan(Math.toRadians(latitude));
        final double cosU1 = 1 / Math.sqrt(1 + tanU1 * tanU1), sinU1 = tanU1 * cosU1;
        final double sigma1 = Math.atan2(tanU1, cosAlpha1);
        final double sinAlpha = cosU1 * sinAlpha1;
        final double cosSqAlpha = 1 - sinAlpha * sinAlpha;
        final double uSq = cosSqAlpha * (semiMajorAxis * semiMajorAxis - b * b) / (b * b);
        final double aa = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
        final double ab = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));

        double sigma = distanceInMeters / (b * aa);
        double sigmaP = 2 * Math.PI;
        double sinSigma = 0;
        double cosSigma = 0;
        double cos2SigmaM = 0;
        double deltaSigma = 0;
        while (Math.abs(sigma - sigmaP) > 1e-12) {
            cos2SigmaM = Math.cos(2 * sigma1 + sigma);
            sinSigma = Math.sin(sigma);
            cosSigma = Math.cos(sigma);
            deltaSigma = ab
                    * sinSigma
                    * (cos2SigmaM + ab
                    / 4
                    * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - ab / 6 * cos2SigmaM
                    * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
            sigmaP = sigma;
            sigma = distanceInMeters / (b * aa) + deltaSigma;
        }

        final double tmp = sinU1 * sinSigma - cosU1 * cosSigma * cosAlpha1;
        final double lat2 = Math.atan2(sinU1 * cosSigma + cosU1 * sinSigma * cosAlpha1,
                (1 - inverseFlattening) * Math.sqrt(sinAlpha * sinAlpha + tmp * tmp));
        final double lambda = Math.atan2(sinSigma * sinAlpha1, cosU1 * cosSigma - sinU1 * sinSigma * cosAlpha1);
        final double c = inverseFlattening / 16 * cosSqAlpha * (4 + inverseFlattening * (4 - 3 * cosSqAlpha));
        final double l = lambda - (1 - c) * inverseFlattening * sinAlpha
                * (sigma + c * sinSigma * (cos2SigmaM + c * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));

        return new LatLng(Math.toDegrees(lat2), longitude + Math.toDegrees(l));
    }
}
