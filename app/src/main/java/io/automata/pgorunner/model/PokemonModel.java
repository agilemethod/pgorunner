package io.automata.pgorunner.model;

public interface PokemonModel {
    String getPokemonText();
}
