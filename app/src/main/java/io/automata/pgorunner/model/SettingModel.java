package io.automata.pgorunner.model;

public interface SettingModel {

    String SERVICE_ENABLE = "SERVICE_ENABLE";
    String FAKE_LOC_CHANGE = "FAKE_LOC_CHANGE";
    String FAKE_LOC_RESET = "FAKE_LOC_RESET";
    String FAKE_LOC_DRAGGABLE = "FAKE_LOC_DRAGGABLE";
    String POKEMON_MAP_ENABLE = "POKEMON_MAP_ENABLE";
    String FLOATING_MAP_ENABLE = "FLOATING_MAP_ENABLE";
}
