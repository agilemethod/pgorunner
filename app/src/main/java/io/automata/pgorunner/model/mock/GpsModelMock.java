package io.automata.pgorunner.model.mock;

import android.location.Criteria;
import android.location.Location;

import io.automata.pgorunner.model.GpsModel;

public class GpsModelMock implements GpsModel {

    private final Location mHomeLocation;
    private final Location mFakeLocation;
    private EventListener mEventListener;

    public GpsModelMock() {
        mHomeLocation = new Location("gps");
        mHomeLocation.setTime(System.currentTimeMillis());
        mHomeLocation.setLatitude(25.047565);
        mHomeLocation.setLongitude(121.516996);
        mHomeLocation.setAccuracy(Criteria.ACCURACY_COARSE);

        mFakeLocation = new Location("gps");
        mFakeLocation.setTime(System.currentTimeMillis());
        mFakeLocation.setLatitude(25.047565);
        mFakeLocation.setLongitude(121.516996);
        mFakeLocation.setAccuracy(Criteria.ACCURACY_COARSE);
    }

    @Override
    public Location getHomeLocation() {
        return mHomeLocation;
    }

    @Override
    public Location getFakeLocation() {
        return mFakeLocation;
    }

    @Override
    public void setFakeLocation(double latitude, double longitude) {
        if (mFakeLocation.getLatitude() != latitude && mFakeLocation.getLongitude() != longitude) {
            mFakeLocation.setLatitude(latitude);
            mFakeLocation.setLongitude(longitude);

            if (mEventListener != null) {
                mEventListener.OnFakeLocationChanged();
            }
        }
    }

    @Override
    public void setEventListener(EventListener eventListener) {
        mEventListener = eventListener;
    }

}
