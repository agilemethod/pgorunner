package io.automata.pgorunner.model.mock;

import io.automata.pgorunner.model.PokemonModel;

public class PokemonModelMock implements PokemonModel {

    private final String mPokemonText = "{\"pokemons\":[{\"id\":-8410605429510914304,\"pokemon_id\":19,\"cp\":55,\"stamina_max\":19,\"individual_attack\":8,\"individual_defense\":10,\"individual_stamina\":15,\"creation_time_ms\":1472271841350,\"move_1\":219,\"move_2\":129},{\"id\":3821392690307545409,\"pokemon_id\":19,\"cp\":10,\"stamina_max\":10,\"individual_attack\":9,\"individual_defense\":1,\"individual_stamina\":15,\"creation_time_ms\":1472048482921,\"move_1\":219,\"move_2\":131},{\"id\":-4701121864914408246,\"pokemon_id\":27,\"cp\":91,\"stamina_max\":33,\"individual_attack\":5,\"individual_defense\":1,\"individual_stamina\":14,\"creation_time_ms\":1472280238802,\"move_1\":216,\"move_2\":63},{\"id\":1198768860000272312,\"pokemon_id\":16,\"cp\":25,\"stamina_max\":15,\"individual_attack\":4,\"individual_defense\":2,\"individual_stamina\":13,\"creation_time_ms\":1472046548352,\"move_1\":219,\"move_2\":80},{\"id\":-1381737712633901071,\"pokemon_id\":7,\"cp\":134,\"stamina_max\":30,\"individual_attack\":0,\"individual_defense\":3,\"individual_stamina\":6,\"creation_time_ms\":1472280849307,\"move_1\":221,\"move_2\":105},{\"id\":6485406039746797699,\"pokemon_id\":8,\"cp\":141,\"stamina_max\":30,\"individual_attack\":0,\"individual_defense\":11,\"individual_stamina\":3,\"creation_time_ms\":1472060783676,\"move_1\":230,\"move_2\":107},{\"id\":2783174068814665930,\"pokemon_id\":129,\"cp\":37,\"stamina_max\":15,\"individual_attack\":11,\"individual_defense\":11,\"individual_stamina\":9,\"creation_time_ms\":1472281166707,\"move_1\":231,\"move_2\":133}]}";
    private int mRequestCounter = 0;

    @Override
    public String getPokemonText() {
        if (++mRequestCounter % 2 != 0) {
            return mPokemonText;
        } else {
            return "{\"pokemons\":[]}";
        }
    }
}
