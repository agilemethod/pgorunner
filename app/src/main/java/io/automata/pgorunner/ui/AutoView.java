package io.automata.pgorunner.ui;

import android.view.LayoutInflater;
import android.view.WindowManager;

public interface AutoView {

    void show();

    void hide();

    void close();

    void destroy();

    boolean getVisible();

    void init(WindowManager mWindowManager, LayoutInflater mLayoutInflater);
}
