package io.automata.pgorunner.ui;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.RelativeLayout;

public abstract class AutoViewBase extends RelativeLayout implements AutoView {

    private final static Handler autoViewBaseTimerHandler = new Handler();
    private boolean mVisible;
    private boolean mWindowAdded;
    protected WindowManager mWindowManager;
    protected LayoutInflater mLayoutInflater;

    public AutoViewBase(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mVisible = false;
    }

    @Override
    public final void init(WindowManager windowManager, LayoutInflater layoutInflater) {
        mWindowManager = windowManager;
        mLayoutInflater = layoutInflater;
        mWindowAdded = false;

        handleInit();
    }

    @Override
    public final void show() {
        if (!mWindowAdded) {
            mWindowManager.addView(this, getShowLayoutParams());
            mWindowAdded = true;
        } else {
            mWindowManager.updateViewLayout(this, getShowLayoutParams());
        }

        handleShow();
        setVisible(true);
    }

    @Override
    public final void hide() {
        // note: that child doesn't override getHideLayoutParams() means to use close() default
        WindowManager.LayoutParams hideLayoutParams = getHideLayoutParams();

        if (!mWindowAdded) {
            if (hideLayoutParams != null) {
                mWindowManager.addView(this, hideLayoutParams);
                mWindowAdded = true;
            }
        } else {
            if (hideLayoutParams != null) {
                mWindowManager.updateViewLayout(this, hideLayoutParams);
            } else {
                close();
            }
        }

        handleHide();
        setVisible(false);
    }

    @Override
    public final void close() {
        if (mWindowAdded) {
            mWindowManager.removeView(this);
            mWindowAdded = false;
        }

        handleClose();
    }

    @Override
    public void destroy() {
        close();
        handleDestroy();
    }

    @Override
    public final boolean getVisible() {
        return mVisible;
    }

    private void setVisible(boolean visible) {
        if (mVisible != visible) {
            mVisible = visible;
            handleVisibleChanged(mVisible);
        }
    }

    protected final void update() {
        mWindowManager.updateViewLayout(this, getShowLayoutParams());
    }

    protected abstract WindowManager.LayoutParams getShowLayoutParams();

    protected WindowManager.LayoutParams getHideLayoutParams() {
        return null;
    }

    protected abstract void handleInit();

    protected void handleShow() {
    }

    protected void handleHide() {
    }

    protected void handleClose() {
    }

    protected void handleDestroy() {
    }

    protected void handleVisibleChanged(boolean visible) {
    }

    protected final void setTimeout(Runnable task, long delayMillis) {
        autoViewBaseTimerHandler.postDelayed(task, delayMillis);
    }

    protected final void clearTimeout(Runnable task) {
        autoViewBaseTimerHandler.removeCallbacks(task);
    }
}
