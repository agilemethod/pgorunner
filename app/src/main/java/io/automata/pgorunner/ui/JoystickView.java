package io.automata.pgorunner.ui;

public interface JoystickView extends AutoView {

    void setEventListener(EventListener eventListener);

    double getDegree();

    void setDraggable(boolean set);

    interface EventListener {
        void OnJoystickUpdated();
    }
}
