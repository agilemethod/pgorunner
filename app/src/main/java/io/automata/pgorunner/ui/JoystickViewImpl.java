package io.automata.pgorunner.ui;

import android.content.Context;
import android.graphics.PixelFormat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import io.automata.pgorunner.R;

public class JoystickViewImpl extends AutoViewBase implements JoystickView {

    private WindowManager.LayoutParams mShowLayoutParams;
    private EventListener mEventListener;
    private View mBase;
    private View mHandle;
    float mBaseRadius;
    float mBaseRadiusSquare;
    float mHandleRadius;
    float mOffset;
    private Runnable mSamplingTask;
    private boolean mViewIsDraggable = false;
    private final int mJoystickWidth;

    public JoystickViewImpl(Context context) {
        this(context, null);
    }

    public JoystickViewImpl(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public JoystickViewImpl(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mJoystickWidth = (int) getResources().getDimension(R.dimen.g_joystick_width);
    }

    @Override
    public void onLayout (boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (mBase == null || mHandle == null) {
            mBase = findViewById(R.id.base);
            mHandle = findViewById(R.id.handle);
            mBaseRadius = mBase.getWidth() / 2;
            mBaseRadiusSquare = mBaseRadius * mBaseRadius;
            mHandleRadius = mHandle.getWidth() / 2;
            mOffset = (getWidth() - mBase.getWidth()) / 2;
            setHandleX(mBaseRadius);
            setHandleY(mBaseRadius);
            mHandle.setVisibility(mViewIsDraggable ? INVISIBLE : VISIBLE);
        }
    }

    @Override
    protected WindowManager.LayoutParams getShowLayoutParams() {
        if (mShowLayoutParams == null) {
            mShowLayoutParams = new WindowManager.LayoutParams(
                    (int) getResources().getDimension(R.dimen.g_joystick_width),
                    (int) getResources().getDimension(R.dimen.g_joystick_width),
                    WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSLUCENT);
            mShowLayoutParams.gravity = Gravity.TOP | Gravity.LEFT;
            mShowLayoutParams.x = (int) getResources().getDimension(R.dimen.g_btn_width);
            mShowLayoutParams.y = (int) getResources().getDimension(R.dimen.g_joystick_width);
        }

        return mShowLayoutParams;
    }

    @Override
    protected void handleInit() {
        mSamplingTask = new Runnable() {
            @Override public void run() { sample(); }
        };

        initTouchEvent();
    }

    private void initTouchEvent() {
        this.setOnTouchListener(new OnTouchListener() {

            // method object
            class TouchHandler {

                private final WindowManager.LayoutParams layoutParams = getShowLayoutParams();
                private int left = 0;
                private int top = 0;
                private int startX = 0;
                private int startY = 0;

                public void handleDragJoystick(MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            left = layoutParams.x;
                            top = layoutParams.y;
                            startX = (int) event.getRawX();
                            startY = (int) event.getRawY();
                            break;
                        case MotionEvent.ACTION_MOVE:
                        case MotionEvent.ACTION_UP:
                            final int dX = (int) event.getRawX() - startX;
                            final int dY = (int) event.getRawY() - startY;
                            layoutParams.x = left + dX;
                            layoutParams.y = top + dY;

                            update();
                    }
                }

                public void handleTouchJoystick(MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_MOVE) {
                        sendPosition(event.getX(), event.getY());
                    } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        sendPosition(event.getX(), event.getY());
                        startSampling();
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        stopSampling();
                        sendPosition(getWidth() / 2, getHeight() / 2);
                    }
                }
            }

            final TouchHandler touchHandler = new TouchHandler();

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (mViewIsDraggable) {
                    touchHandler.handleDragJoystick(event);
                } else {
                    touchHandler.handleTouchJoystick(event);
                }
                return false;
            }
        });
    }

    private void startSampling() {
        clearTimeout(mSamplingTask);
        setTimeout(mSamplingTask, 200);
    }

    private void stopSampling() {
        clearTimeout(mSamplingTask);
    }

    private void sample() {
        if (mEventListener != null) {
            setTimeout(mSamplingTask, 1000);
            mEventListener.OnJoystickUpdated();
        }
    }

    private void sendPosition(float x, float y) {
        x -= mOffset;
        y -= mOffset;
        float xFromOrigin = x - mBaseRadius;
        float yFromOrigin = y - mBaseRadius;

        float distanceFromOrigin = xFromOrigin * xFromOrigin + yFromOrigin * yFromOrigin;
        if (distanceFromOrigin > mBaseRadiusSquare) {
            double angle = Math.atan2(yFromOrigin, xFromOrigin);
            x = (float)(Math.cos(angle) * mBaseRadius) + mBaseRadius;
            y = (float)(Math.sin(angle) * mBaseRadius) + mBaseRadius;
        }

        setHandleX(x);
        setHandleY(y);
    }

    @Override
    public void setEventListener(EventListener eventListener) {
        mEventListener = eventListener;
    }

    @Override
    public double getDegree() {
        double xFromOrigin = getHandleX() - mBaseRadius;
        double yFromOrigin = getHandleY() - mBaseRadius;

        return -Math.toDegrees(Math.atan2(-yFromOrigin, xFromOrigin)) + 90;
    }

    @Override
    public void setDraggable(boolean set) {
        mViewIsDraggable = set;
        if (mHandle != null) {
            mHandle.setVisibility(mViewIsDraggable ? INVISIBLE : VISIBLE);
        }
    }

    private void setHandleX(float x) {
        mHandle.setX(x - mHandleRadius);
    }

    private void setHandleY(float y) {
        mHandle.setY(y - mHandleRadius);
    }

    private float getHandleX() {
        return mHandle.getX() + mHandleRadius;
    }

    private float getHandleY() {
        return mHandle.getY() + mHandleRadius;
    }
}
