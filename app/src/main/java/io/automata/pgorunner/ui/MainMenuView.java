package io.automata.pgorunner.ui;

public interface MainMenuView extends AutoView {

    enum IconType {
        MAP, IV, SPEED, NAVIGATE, CLOSE
    }

    void setEventListener(EventListener eventListener);

    int getMovingSpeed();

    void setState(IconType type, int state);

    interface EventListener {

        void OnVisibleChanged();

        void OnMapIconClicked();

        void OnIvIconClicked();

        void OnSpeedIconClicked();

        void OnNavigateIconClicked();

        void OnCloseIconClicked();

    }
}
