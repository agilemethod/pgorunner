package io.automata.pgorunner.ui;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;

import java.util.ArrayList;

import io.automata.pgorunner.R;

public class MainMenuViewImpl extends AutoViewBase implements MainMenuView {

    private EventListener mEventListener;
    private WindowManager.LayoutParams mShowLayoutParams;
    private WindowManager.LayoutParams mHideLayoutParams;
    private ArrayList<MainMenuIcon> mIcons;

    public MainMenuViewImpl(Context context) {
        this(context, null);
    }

    public MainMenuViewImpl(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainMenuViewImpl(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected WindowManager.LayoutParams getShowLayoutParams() {
        if (mShowLayoutParams == null) {
            mShowLayoutParams = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    //(int)getResources().getDimension(R.dimen.main_menu_button_height),
                    (int)getResources().getDimension(R.dimen.g_btn_width),
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSLUCENT);

            mShowLayoutParams.gravity = Gravity.TOP | Gravity.LEFT;
            mShowLayoutParams.x = 0;
            mShowLayoutParams.y = 0;
            mShowLayoutParams.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        }

        return mShowLayoutParams;
    }

    @Override
    protected WindowManager.LayoutParams getHideLayoutParams() {
        if (mHideLayoutParams == null) {
            mHideLayoutParams = new WindowManager.LayoutParams(
                    (int) getResources().getDimension(R.dimen.g_btn_width),
                    (int) getResources().getDimension(R.dimen.g_btn_width),
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSLUCENT);

            mHideLayoutParams.gravity = Gravity.TOP | Gravity.LEFT;
            mHideLayoutParams.x = 0;
            mHideLayoutParams.y = 0;
            mHideLayoutParams.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
        }

        return mHideLayoutParams;
    }

    @Override
    protected void handleInit() {
        ImageButton mainButton = (ImageButton) findViewById(R.id.btn_main);
        mainButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getVisible()) {
                    hide();
                } else {
                    show();
                }
            }
        });

        final MainMenuViewImpl self = this;
        mainButton.setOnTouchListener(new OnTouchListener() {
            public int left = 0;
            public int startX = 0;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!getVisible()) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            left = mHideLayoutParams.x;
                            startX = (int) event.getRawX();
                            break;
                        case MotionEvent.ACTION_MOVE:
                        case MotionEvent.ACTION_UP:
                            final int dX = (int) event.getRawX() - startX;
                            mHideLayoutParams.x = left + dX;
                            mWindowManager.updateViewLayout(self, mHideLayoutParams);
                            break;
                    }
                }
                return false;
            }
        });

        mIcons = new ArrayList<>();
        mIcons.add(new MapIcon(this));
        mIcons.add(new IvIcon(this));
        mIcons.add(new SpeedIcon(this));
        mIcons.add(new NavigateIcon(this));
        mIcons.add(new CloseIcon(this));
    }

    @Override
    protected void handleVisibleChanged(boolean visible) {
        if (mEventListener != null) {
            mEventListener.OnVisibleChanged();
        }
    }

    @Override
    public void onFinishInflate() {
        super.onFinishInflate();
    }

    // =======================================================
    // methods
    @Override
    public void setEventListener(EventListener eventListener) {
        mEventListener = eventListener;
    }

    @Override
    public int getMovingSpeed() {
        SpeedIcon speedIcon = (SpeedIcon)mIcons.get(IconType.SPEED.ordinal());
        return speedIcon.mSpeed;
    }

    @Override
    public void setState(IconType type, int state) {
        mIcons.get(type.ordinal()).setState(state);
    }

    // =======================================================
    // helpers
    private abstract class MainMenuIcon {

        protected final MainMenuViewImpl mMainMenuViewImpl;
        protected int mState;

        public MainMenuIcon(MainMenuViewImpl mainMenuViewImpl) {
            mMainMenuViewImpl = mainMenuViewImpl;
            mState = -1;
        }

        public void setState(int state) {
            mState = state;
        }
    }

    // =======================================================
    // implement MapIcon
    private class MapIcon extends MainMenuIcon {

        private final ImageButton mButton;

        public MapIcon(MainMenuViewImpl mainMenuViewImpl) {
            super(mainMenuViewImpl);

            mButton = (ImageButton) mMainMenuViewImpl.findViewById(R.id.menu_map_btn);
            mButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mMainMenuViewImpl.mEventListener != null) {
                        mMainMenuViewImpl.mEventListener.OnMapIconClicked();
                    }
                }
            });
        }
    }

    // =======================================================
    // implement IvIcon
    private class IvIcon extends MainMenuIcon {

        private final ImageButton mButton;

        public IvIcon(MainMenuViewImpl mainMenuViewImpl) {
             super(mainMenuViewImpl);

             mButton = (ImageButton) mMainMenuViewImpl.findViewById(R.id.menu_iv_btn);
             mButton.setOnClickListener(new OnClickListener() {
                 @Override
                 public void onClick(View view) {
                     if (mMainMenuViewImpl.mEventListener != null) {
                         mMainMenuViewImpl.mEventListener.OnIvIconClicked();
                     }
                 }
             });
        }
    }

    // =======================================================
    // implement SpeedIcon
    private class SpeedIcon extends MainMenuIcon {

        private final int SPEED_COUNT = 5;
        private final int[] mSpeedResource = {R.drawable.speed_1x, R.drawable.speed_2x, R.drawable.speed_4x, R.drawable.speed_6x, R.drawable.speed_8x};
        private final ImageButton mButton;
        private int mSpeed = 1;

        public SpeedIcon(MainMenuViewImpl mainMenuViewImpl) {
            super(mainMenuViewImpl);
            mState = 0;
            mSpeed = 1;

            mButton = (ImageButton) mMainMenuViewImpl.findViewById(R.id.menu_speed_btn);
            mButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    setState((mState + 1) % SPEED_COUNT);

                    if (mMainMenuViewImpl.mEventListener != null) {
                        mMainMenuViewImpl.mEventListener.OnSpeedIconClicked();
                    }
                }
            });
        }

        @Override
        public void setState(int state) {
            if (mState != state) {
                mState = state;
                mSpeed = mState * 2;
                if (mSpeed == 0) {
                    mSpeed = 1;
                }

                mButton.setBackground(mMainMenuViewImpl.getContext().getResources().getDrawable(mSpeedResource[mState]));
            }
        }
    }

    // =======================================================
    // implement NavigateIcon
    private class NavigateIcon extends MainMenuIcon {

        private final ImageButton mButton;
        private int mState;

        public NavigateIcon(MainMenuViewImpl mainMenuView) {
            super(mainMenuView);
            mState = 0;

            mButton = (ImageButton) mMainMenuViewImpl.findViewById(R.id.menu_navigation_btn);
            mButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mMainMenuViewImpl.mEventListener != null) {
                        mMainMenuViewImpl.mEventListener.OnNavigateIconClicked();
                    }
                }
            });
        }

        @Override
        public void setState(int state) {
            if (mState != state) {
                mState = state;

                if (mState == 0) {
                    mButton.setBackground(mMainMenuViewImpl.getContext().getResources().getDrawable(R.drawable.navigation));
                } else {
                    mButton.setBackground(mMainMenuViewImpl.getContext().getResources().getDrawable(R.drawable.navigation_enabled));
                }
            }
        }
    }

    // =======================================================
    // implement CloseIcon
    private class CloseIcon extends MainMenuIcon {

        private final ImageButton mButton;

        public CloseIcon(final MainMenuViewImpl mainMenuView) {
            super(mainMenuView);

            mButton = (ImageButton) mMainMenuViewImpl.findViewById(R.id.menu_close_btn);
            mButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mMainMenuViewImpl.mEventListener != null) {
                        mMainMenuViewImpl.mEventListener.OnCloseIconClicked();
                    }
                }
            });
        }
    }
}
