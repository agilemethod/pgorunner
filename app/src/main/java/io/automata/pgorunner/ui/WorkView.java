package io.automata.pgorunner.ui;

import android.location.Location;

import com.google.android.gms.maps.model.LatLngBounds;

public interface WorkView extends AutoView {

    enum ModeType {
        MAP, IV, UNKNOWN
    }

    void setMode(ModeType mode);

    ModeType getMode();

    void setEventListener(EventListener eventListener);

    interface EventListener {
        void OnMapWalkButtonClicked();

        void OnTargetLocationChanged();

        void OnMapCameraIdle(LatLngBounds bounds);

        void OnFakeLocationChanged();
    }

    // =======================================================
    // MAP
    Location getTargetLocation();

    void setFakeLocation(Location fakeLocation);

    void setTargetLocation(Location targetLocation);

    // =======================================================
    // IV
    void setPokemonText(String pokemonText);
}
