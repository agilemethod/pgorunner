package io.automata.pgorunner.ui;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.aeonlucid.pogoprotos.Enums;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.automata.pgorunner.R;
import io.automata.pgorunner.ui.tool.IvListViewArrayAdapter;
import io.automata.pgorunner.utility.PokemonInfo;

public class WorkViewImpl extends AutoViewBase implements WorkView {

    private WindowManager.LayoutParams mShowLayoutParams;
    private MapContainer mMapContainer;
    private IvContainer mIvContainer;
    private ModeType mMode = ModeType.UNKNOWN;
    private EventListener mEventListener;

    public WorkViewImpl(Context context) {
        this(context, null);
    }

    public WorkViewImpl(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WorkViewImpl(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected WindowManager.LayoutParams getShowLayoutParams() {
        if (mShowLayoutParams == null) {
            Point screen = new Point();
            mWindowManager.getDefaultDisplay().getSize(screen);

            int height = screen.y - (int) getResources().getDimension(R.dimen.work_view_y);
            mShowLayoutParams = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.MATCH_PARENT,
                    height,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSLUCENT);
            mShowLayoutParams.gravity = Gravity.TOP | Gravity.LEFT;
            mShowLayoutParams.x = 0;
            mShowLayoutParams.y = (int) getResources().getDimension(R.dimen.work_view_y);
        }

        return mShowLayoutParams;
    }

    @Override
    protected void handleInit() {
        mMapContainer = new MapContainer(this);
        mIvContainer = new IvContainer(this);
        setMode(ModeType.MAP);
    }

    @Override
    protected void handleDestroy() {
        mMapContainer.mMapView.onDestroy();
    }

    @Override
    public void setEventListener(WorkView.EventListener eventListener) { mEventListener = eventListener; }

    @Override
    public void setMode(ModeType mode) {
        if (mMode != mode) {
            mMode = mode;
            if (mMode == ModeType.MAP) {
                mIvContainer.hide();
                mMapContainer.show();
            } else {
                mMapContainer.hide();
                mIvContainer.show();
            }
        }
    }

    @Override
    public ModeType getMode() {
        return mMode;
    }

    @Override
    public Location getTargetLocation() {
        return mMapContainer.mTargetLocation;
    }

    @Override
    public void setFakeLocation(Location fakeLocation) {
        mMapContainer.setFakeLocation(fakeLocation);
    }

    @Override
    public void setTargetLocation(Location targetLocation) {
        mMapContainer.setFakeLocation(targetLocation);
    }

    @Override
    public void setPokemonText(String pokemonText) {
        mIvContainer.setPokemonText(pokemonText);
    }

    // =======================================================
    // helper
    private abstract class WorkContainer {
        protected WorkViewImpl mWorkView;

        public WorkContainer(WorkViewImpl workView) {
            mWorkView = workView;
        }

        public abstract void show();

        public abstract void hide();
    }

    // =======================================================
    // implement MapContainer
    private class MapContainer extends WorkContainer {
        private final RelativeLayout mMapLayout;
        private final Button mMapWalkButton;
        private final MapView mMapView;
        private Location mTargetLocation = new Location("gps");
        private Location mFakeLocation = new Location("gps");
        private GoogleMap mGoogleMap;
        private Marker mTargetLocMarker;
        private Marker mFakeLocMarker;
        private boolean mIsShow = false;

        public MapContainer(WorkViewImpl workView) {
            super(workView);

            mMapLayout = (RelativeLayout) mWorkView.findViewById(R.id.work_map_container);
            mMapWalkButton = (Button) mMapLayout.findViewById(R.id.map_walk_button);
            mMapWalkButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mWorkView.mEventListener != null) {
                        mWorkView.mEventListener.OnMapWalkButtonClicked();
                    }
                }
            });

            mMapView = (MapView) mWorkView.findViewById(R.id.map);
            initMap();
        }

        private void initMap() {
            mMapView.onCreate(new Bundle());

            mMapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(final GoogleMap googleMap) {
                    mGoogleMap = googleMap;
                    googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                    MapsInitializer.initialize(getContext());
                    if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) ==
                            PackageManager.PERMISSION_GRANTED) {
                        googleMap.setMyLocationEnabled(true);
                    }

                    mTargetLocMarker = googleMap.addMarker(
                            new MarkerOptions()
                                    .title("TargetLocation")
                                    .position(new LatLng(mTargetLocation.getLatitude(), mTargetLocation.getLongitude()))
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                                    .draggable(true)
                    );
                    mTargetLocMarker.setTag("Target");

                    mFakeLocMarker = googleMap.addMarker(
                            new MarkerOptions()
                                    .title("FakeLocation")
                                    .position(new LatLng(mFakeLocation.getLatitude(), mFakeLocation.getLongitude()))
                                    .draggable(true)
                    );
                    mFakeLocMarker.setTag("Fake");

                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(mFakeLocation.getLatitude(), mFakeLocation.getLongitude()), 16);
                    googleMap.animateCamera(cameraUpdate);

                    googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                        @Override public void onMarkerDragStart(Marker marker) {}

                        @Override public void onMarkerDrag(Marker marker) {}

                        @Override public void onMarkerDragEnd(Marker marker) {
                            if (marker.getTag().equals("Target")) {
                                LatLng latLng = mTargetLocMarker.getPosition();
                                Location targetLocation = new Location("gps");
                                targetLocation.setLatitude(latLng.latitude);
                                targetLocation.setLongitude(latLng.longitude);
                                setTargetLocation(targetLocation);
                            } else if (marker.getTag().equals("Fake")) {
                                LatLng latLng = mFakeLocMarker.getPosition();
                                Location fakeLocation = new Location("gps");
                                fakeLocation.setLatitude(latLng.latitude);
                                fakeLocation.setLongitude(latLng.longitude);
                                setFakeLocation(fakeLocation);
                            }
                        }
                    });

                    googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                        @Override public void onMapLongClick(LatLng latLng) {
                            mTargetLocMarker.setPosition(latLng);
                            Location targetLocation = new Location("gps");
                            targetLocation.setLatitude(latLng.latitude);
                            targetLocation.setLongitude(latLng.longitude);
                            setTargetLocation(targetLocation);
                        }
                    });

                    googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                        @Override public void onCameraIdle() {
                            final LatLngBounds bounds = googleMap.getProjection().getVisibleRegion().latLngBounds;
                            if (mWorkView.mEventListener != null) {
                                mWorkView.mEventListener.OnMapCameraIdle(bounds);
                            }
                        }
                    });
                }
            });
        }

        public void setTargetLocation(Location targetLocation) {
            if (mTargetLocation.getLatitude() != targetLocation.getLatitude() && mTargetLocation.getLongitude() != targetLocation.getLongitude()) {
                mTargetLocation.setLatitude(targetLocation.getLatitude());
                mTargetLocation.setLongitude(targetLocation.getLongitude());

                if (mIsShow && mTargetLocMarker != null) {
                    updateTargetMarker();
                }

                if (mWorkView.mEventListener != null) {
                    mWorkView.mEventListener.OnTargetLocationChanged();
                }
            }
        }

        public void setFakeLocation(Location fakeLocation) {
            if (mFakeLocation.getLatitude() != fakeLocation.getLatitude() && mFakeLocation.getLongitude() != fakeLocation.getLongitude()) {
                mFakeLocation.setLatitude(fakeLocation.getLatitude());
                mFakeLocation.setLongitude(fakeLocation.getLongitude());

                if (mIsShow && mFakeLocMarker != null) {
                    updateFakeMarker();
                }

                if (mWorkView.mEventListener != null) {
                    mWorkView.mEventListener.OnFakeLocationChanged();
                }
            }
        }

        @Override
        public void show() {
            mIsShow = true;
            mMapLayout.setVisibility(VISIBLE);
            mMapView.setVisibility(VISIBLE);
            mMapView.onResume();

            if (mTargetLocMarker != null) {
                updateTargetMarker();
            }
            if (mFakeLocMarker != null) {
                updateFakeMarker();
            }
        }

        private void updateTargetMarker() {
            mTargetLocMarker.setPosition(new LatLng(mTargetLocation.getLatitude(), mTargetLocation.getLongitude()));
        }

        private void updateFakeMarker() {
            mFakeLocMarker.setPosition(new LatLng(mFakeLocation.getLatitude(), mFakeLocation.getLongitude()));
        }

        @Override
        public void hide() {
            mIsShow = false;
            mMapLayout.setVisibility(GONE);
            mMapView.setVisibility(GONE);
            mMapView.onPause();
        }
    }

    // =======================================================
    // implement IvContainer
    private class IvContainer extends WorkContainer {
        private final RelativeLayout mIvLayout;
        private final ListView mIvListView;
        private final TextView mHintView;
        private final IvListViewArrayAdapter mIvListViewArrayAdapter;
        private final Button mIvSortButton;
        private final int mFieldMAXSIZE;

        public IvContainer(WorkViewImpl workView) {
            super(workView);

            mIvLayout = (RelativeLayout) mWorkView.findViewById(R.id.work_iv_container);
            mHintView = (TextView) mIvLayout.findViewById(R.id.pokemon_help);
            mIvListView = (ListView) mIvLayout.findViewById(R.id.pokemon_list);
            mIvListViewArrayAdapter = new IvListViewArrayAdapter(getContext(), new ArrayList<PokemonInfo>());
            mFieldMAXSIZE = IvListViewArrayAdapter.FieldType.MAXSIZE.ordinal();
            mIvListView.setAdapter(mIvListViewArrayAdapter);
            mIvSortButton = (Button) mIvLayout.findViewById(R.id.pokemon_sort_button);
            mIvSortButton.setOnClickListener(new Button.OnClickListener(){
                @Override
                public void onClick(View v) {
                    int sortType = mIvListViewArrayAdapter.sortType().ordinal();
                    mIvListViewArrayAdapter.sortBy(IvListViewArrayAdapter.FieldType.values()[(sortType + 1) % mFieldMAXSIZE]);
                    mIvListViewArrayAdapter.notifyDataSetChanged();
                    mIvSortButton.setText("sort By " + mIvListViewArrayAdapter.sortName());
                }
            });

            mIvListViewArrayAdapter.sortBy(IvListViewArrayAdapter.FieldType.IV);
            mIvSortButton.setText("sort By " + mIvListViewArrayAdapter.sortName());
        }

        @Override
        public void show() {
            mIvLayout.setVisibility(VISIBLE);
        }

        @Override
        public void hide() {
            mIvLayout.setVisibility(GONE);
        }

        public void setPokemonText(String pokemonText) {
            ArrayList<PokemonInfo> pokemonList;
            try {
                pokemonList = parsePokemonJson(pokemonText);
                if (pokemonList.size() == 0) {
                    displayHint();
                } else {
                    displayIvList();
                    mIvListViewArrayAdapter.updateDataSet(pokemonList);
                    mIvListViewArrayAdapter.notifyDataSetChanged();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        private void displayHint() {
            mHintView.setVisibility(VISIBLE);
            mIvListView.setVisibility(GONE);
        }

        private void displayIvList() {
            mHintView.setVisibility(GONE);
            mIvListView.setVisibility(VISIBLE);
        }

        private ArrayList<PokemonInfo> parsePokemonJson(String text) throws JSONException {
            ArrayList<PokemonInfo> result = new ArrayList<>();

            JSONObject jPokemonPackage = new JSONObject(text);
            JSONArray jPokemonArray = jPokemonPackage.getJSONArray("pokemons");

            for (int i = 0; i < jPokemonArray.length(); i++) {
                JSONObject jInfo = jPokemonArray.getJSONObject(i);
                PokemonInfo info = new PokemonInfo();
                info.pokemonId = jInfo.getInt("pokemon_id");
                info.cp = jInfo.getInt("cp");
                info.staminaMax = jInfo.getInt("stamina_max");
                info.attack = jInfo.getInt("individual_attack");
                info.defense = jInfo.getInt("individual_defense");
                info.stamina = jInfo.getInt("individual_stamina");
                info.creationTimeMs = jInfo.getLong("creation_time_ms");
                info.move1 = jInfo.getInt("move_1");
                info.move2 = jInfo.getInt("move_2");
                if (jInfo.has("nickname")) {
                    info.nickName = jInfo.getString("nickname");
                    Enums.PokemonId pid = Enums.PokemonId.forNumber(info.pokemonId);
                    if (pid != null) {
                        info.nickName = pid.name();
                    }
                }
                if (info.nickName == null) {
                    info.nickName = "";
                }
                if (jInfo.has("cp_multiplier")) {
                    double cp_mulitplier = jInfo.getDouble("cp_multiplier");
                    info.setLV(cp_mulitplier);
                } else {
                    info.setLV(0);
                }
                result.add(info);
            }

            return result;
        }
    }
}
