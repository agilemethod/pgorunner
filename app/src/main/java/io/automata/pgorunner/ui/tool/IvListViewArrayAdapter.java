package io.automata.pgorunner.ui.tool;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import io.automata.pgorunner.R;
import io.automata.pgorunner.utility.PokemonInfo;


public class IvListViewArrayAdapter extends ArrayAdapter<PokemonInfo> {
    private final Context context;
    private ArrayList<PokemonInfo> pokemonList;

    public enum FieldType {
        IV, CP, Number, Name, Time, MAXSIZE
    }

    private FieldType mSortType = FieldType.IV;

    public IvListViewArrayAdapter(Context context, ArrayList<PokemonInfo> pokemonList) {
        super(context, R.layout.iv_list_view_item);
        this.context = context;
        this.pokemonList = pokemonList;
    }

    public void updateDataSet(ArrayList<PokemonInfo> pokemonList) {
        this.pokemonList = pokemonList;
    }

    public void sortBy(FieldType type) {
        mSortType = type;
        switch (type) {
            case IV:
                Collections.sort(this.pokemonList, new Comparator<PokemonInfo>() {
                    @Override
                    public int compare(PokemonInfo lhs, PokemonInfo rhs) {
                        return lhs.ivRatio() < rhs.ivRatio() ? 1 : -1;
                    }
                });
                break;
            case CP:
                Collections.sort(this.pokemonList, new Comparator<PokemonInfo>() {
                    @Override
                    public int compare(PokemonInfo lhs, PokemonInfo rhs) {
                        return lhs.cp < rhs.cp ? 1 : -1;
                    }
                });
                break;
            case Number:
                Collections.sort(this.pokemonList, new Comparator<PokemonInfo>() {
                    @Override
                    public int compare(PokemonInfo lhs, PokemonInfo rhs) {
                        return lhs.pokemonId > rhs.pokemonId ? 1 : -1;
                    }
                });
                break;
            case Name:
                Collections.sort(this.pokemonList, new Comparator<PokemonInfo>() {
                    @Override
                    public int compare(PokemonInfo lhs, PokemonInfo rhs) {
                        return lhs.nickName.compareTo(rhs.nickName);
                    }
                });
            case Time:
                Collections.sort(this.pokemonList, new Comparator<PokemonInfo>() {
                    @Override
                    public int compare(PokemonInfo lhs, PokemonInfo rhs) {
                        return lhs.creationTimeMs < rhs.creationTimeMs ? 1 : -1;
                    }
                });
                break;
        }
    }

    @Override
    public int getCount() {
        return this.pokemonList.size();
    }

    @Override
    public PokemonInfo getItem(int position) {
        return this.pokemonList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View ivViewItem = inflater.inflate(R.layout.iv_list_view_item, parent, false);

        ImageView pokeImg = (ImageView) ivViewItem.findViewById(R.id.iv_pokeimg);
        TextView pokeInfo = (TextView) ivViewItem.findViewById(R.id.iv_info);
        TextView pokeAtk = (TextView) ivViewItem.findViewById(R.id.iv_atk);
        TextView pokeDef = (TextView) ivViewItem.findViewById(R.id.iv_def);
        TextView pokeSta = (TextView) ivViewItem.findViewById(R.id.iv_sta);
        TextView pokeRatio = (TextView) ivViewItem.findViewById(R.id.iv_ratio);

        PokemonInfo info = pokemonList.get(position);
        int drawableId = context.getResources().getIdentifier(String.format("ic_pokemon_%03d", info.pokemonId),"drawable", context.getPackageName());
        pokeImg.setImageResource(drawableId);
        pokeInfo.setText(info.nickName + " (LV " + info.lv() + " CP " + info.cp + ")");
        pokeAtk.setText("ATK " + info.attack);
        pokeDef.setText("DEF " + info.defense);
        pokeSta.setText("HP " + info.stamina);
        pokeRatio.setText(String.format("%.1f", info.ivRatio()) + "%");

        //Log.d("getView", "" + position + " " + info.pokemonId + " " + String.format("ic_pokemon_%03d", info.pokemonId));

        return ivViewItem;
    }

    public FieldType sortType() {
        return mSortType;
    }

    public String sortName() {
        return mSortType.toString();
    }
}