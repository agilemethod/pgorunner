package io.automata.pgorunner.utility;

import com.google.android.gms.maps.model.LatLng;

public abstract class Tool {

    public static LatLng latLngFromText(String value) throws RuntimeException {
        String[] pieces = value.split(",");
        if (pieces.length == 2) {
            final float lat = Float.parseFloat(pieces[0]);
            final float lng = Float.parseFloat(pieces[1]);
            return new LatLng(lat, lng);
        }
        throw new RuntimeException("format error");
    }

    public static String textFromLatLng(LatLng latLng) {
        return new String(latLng.latitude + ", " + latLng.longitude);
    }
}
